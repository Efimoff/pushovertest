﻿using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using PushoverTest.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApplicationAPI.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        // POST api/users
        [HttpPost]
        public IActionResult Post([FromBody]MessageScheme mesch)
        {
            if (mesch == null)
            {
                return BadRequest();
            }
            var message = mesch.Message;
            var token = mesch.Token;
            var key = mesch.Key;

            var parameters = new NameValueCollection {
                    { "token", token },
                    { "user", key },
                    { "message", message }
                };

            using (var client = new WebClient())
            {
                client.UploadValues("https://api.pushover.net/1/messages.json", parameters);
            }
            return Ok(mesch);
        }        
    }
}
