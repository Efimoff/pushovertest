﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PushoverTest.Models
{
    public class MessageScheme
    {
        public string Key { get; set; }
        public string Message { get; set; }
        public string Token { get; set; }
    }
}
